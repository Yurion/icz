import { CustomCreateDialog } from './custom-create-dialog/custom-create-dialog.component';
import { DetailFeedComponent } from './detail-feed/detail-feed.component';
import { ExternalLib } from './../Helpers/Modules/ExternalLib.module';
import { FeedsComponent } from './feeds/feeds.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const WELL_KNOWN_COMPONENTS = [
    FeedsComponent,
    CustomCreateDialog,
    DetailFeedComponent,
]

const WELL_KNOWN_MODULES = [
    ExternalLib,
    CommonModule,
    FormsModule,
    ReactiveFormsModule

]
@NgModule({
    imports:
        [
            WELL_KNOWN_MODULES,
        ],
    declarations:
        [
            WELL_KNOWN_COMPONENTS,
           
        ],
    exports:
        [
            WELL_KNOWN_COMPONENTS,
            WELL_KNOWN_MODULES
        ]
    , entryComponents: [
        CustomCreateDialog
    ]
})
export class ComponentsModule { }
