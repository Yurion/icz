import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { interval } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { CondigForDialog } from 'src/app/Helpers/Classes/Helpers';
import { SelectionFeedService } from 'src/app/Helpers/Services/Feed/selection-feed.service';

@Component({
  selector: 'app-custom-create-dialog',
  templateUrl: './custom-create-dialog.component.html',
  styleUrls: ['./custom-create-dialog.component.scss']
})
export class CustomCreateDialog implements OnInit, OnDestroy {
  feedForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private selectFeedServ: SelectionFeedService,
    public dialogRef: MatDialogRef<CustomCreateDialog>,
    @Inject(MAT_DIALOG_DATA) public data: CondigForDialog
  ) { }

  ngOnDestroy(): void {

  }

  ngOnInit(): void {
    this.feedForm = this.fb.group({
      id: null,
      timestamp: null,
      name: ['', Validators.required],
      text: ['', Validators.required],
      likes: 0,
      comments: null,
    })
    if (this.data?.canEdit == true) {
      this.selectFeedServ.updateFeedObs$.pipe(take(1)).subscribe(x => {
        this.feedForm.patchValue(x);
      })
    }
  }
}
