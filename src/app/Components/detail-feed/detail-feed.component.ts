import { CustomCreateDialog } from './../custom-create-dialog/custom-create-dialog.component';
import { Feed, CommentFeed } from './../../Helpers/Classes/Helpers';
import { FeedService } from 'src/app/Services/feed.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { take, reduce, debounceTime, switchMap, map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { InformativeService } from 'src/app/Helpers/Services/Informative/informative.service';

@Component({
  selector: 'app-detail-feed',
  templateUrl: './detail-feed.component.html',
  styleUrls: ['./detail-feed.component.scss']
})
export class DetailFeedComponent implements OnInit {

  @Input() feed: Feed;

  @Output() feedUpdated = new EventEmitter<Feed>();
  @Output() feedCommentDeleted = new EventEmitter<CommentFeed>();

  likeSub$ = new Subject<number>();

  constructor(
    private dialog: MatDialog,
    private feedServ: FeedService,
    private informaServ:InformativeService) { }

  ngOnInit() {

    this.likeSub$.pipe(
      debounceTime(500),
      map(x => this.feed),
      switchMap(x => this.feedServ.saveFeed(x))
    ).subscribe(x => {
      this.informaServ.createToadMessage({severity:'success',detail:'Likes count updated',summary:'Likes'});
        this.feedUpdated.emit(this.feed);
    })
  }

  openDialog() {
    let dialogRef = this.dialog.open(CustomCreateDialog, { data: { buttonText: 'comment' } });
    dialogRef.afterClosed().pipe(take(1)).subscribe((newComment: CommentFeed) => {
      if (newComment) {
        newComment.timestamp = +Date.now();
        this.feedServ.createNewComment(newComment, this.feed.id).pipe(take(1)).subscribe(x => {
          
          this.informaServ.createToadMessage({severity:'success',detail:'Comment created',summary:'Comment'});
          this.feedUpdated.emit(this.feed);
        })
      }
    })
  }
  deleteComment(delComment: CommentFeed) {
    this.feedServ.deleteFeedCommentById(delComment).pipe(take(1)).subscribe(x => {
      this.informaServ.createToadMessage({severity:'success',detail:'Comment deleted',summary:'Comment'});
      this.feedCommentDeleted.emit(delComment)
    })
  }

  modifyLike(feedBack: number) {
    this.feed.likes += feedBack;
    this.likeSub$.next(feedBack);
  }

}
