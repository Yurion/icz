
import { Feed, CommentFeed } from './../../Helpers/Classes/Helpers';
import { tap, catchError, takeUntil, debounceTime, delay, take } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FeedService } from 'src/app/Services/feed.service';
import { Subject, of, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { CustomCreateDialog } from '../custom-create-dialog/custom-create-dialog.component';
import { SelectionFeedService } from 'src/app/Helpers/Services/Feed/selection-feed.service';
import { InformativeService } from 'src/app/Helpers/Services/Informative/informative.service';
@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.scss']
})
export class FeedsComponent implements OnInit, OnDestroy {

  feedsData: Feed[] = [];
  destroy$ = new Subject();
  loadingFeedsData: boolean;
  selectFeedOsb$: Observable<Feed>;
  constructor(
    private feedServ: FeedService,
    private selectionFeedServ: SelectionFeedService,
    private dialog: MatDialog,
    private informServ: InformativeService
  ) { }


  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  ngOnInit() {
    this.selectFeedOsb$ = this.selectionFeedServ.selctedFeedObs$;
    this.loadingFeedsData = true;
    this.selectionFeedServ.feedDataObs$.pipe(
      takeUntil(this.destroy$),
    ).subscribe(x => {
      this.loadingFeedsData = false;
      if (x) {
        this.feedsData = x;
      }
    });


    this.selectionFeedServ.getNewData();
  }

  selectFeed(selectedFeed: Feed) {
    this.selectionFeedServ.selectFeed(selectedFeed);
  }
  openDialog() {
    let dialogRef = this.dialog.open(CustomCreateDialog,{data:{buttonText:'feed'}});
    dialogRef.afterClosed().pipe(take(1)).subscribe((newFeed: Feed) => {
      if (newFeed) {
        newFeed.timestamp = +Date.now();
        this.feedServ.createNewFeed(newFeed).pipe(take(1)).subscribe(x=>{
            ///check and inform for success
            this.informServ.createToadMessage({severity:'success',detail:'Feed created',summary:'Feed'});
            this.selectionFeedServ.getNewData();
        })
      }

    })

  }
  deleteFeed(feedToDelete:Feed){
    this.feedServ.deleteFeed(feedToDelete).pipe(take(1)).subscribe(x=>{
      this.feedsData = this.feedsData.filter(item => item.id !== feedToDelete.id);
      //check if deleted feed was selected
      this.informServ.createToadMessage({severity:'success',detail:'Feed deleted',summary:'Feed'});
      this.selectFeedOsb$.pipe(take(1)).subscribe(s=>{
        if(s.id == feedToDelete.id)
        {
          this.selectionFeedServ.clearSelection();
        }
      })
    })
  }
  editFeed(feedToEdit:Feed){
    this.selectionFeedServ.updatedFeed(feedToEdit);
    let dialogRef = this.dialog.open(CustomCreateDialog,{
      data: {canEdit:true,buttonText: 'feed'}
    });
    dialogRef.afterClosed().pipe(take(1)).subscribe((newFeed: Feed) => {
      if (newFeed) {
        newFeed.timestamp = +Date.now();
        this.feedServ.saveFeed(newFeed).pipe(take(1)).subscribe(x=>{
          this.informServ.createToadMessage({severity:'success',detail:'Feed updated',summary:'Feed'});
            this.selectionFeedServ.getNewData();
        })
      }

    })
  }
  reloardFeed(updatedFeed:Feed){
    this.selectionFeedServ.getNewData();
  }
  deletedCommentFromFeed(comm:CommentFeed){
    this.selectionFeedServ.getNewData();
  }
}
