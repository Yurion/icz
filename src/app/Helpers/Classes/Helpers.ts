export class CommentFeed {
  id: number;
  timestamp: number;
  name: string;
  text: string;
}

export class Feed extends CommentFeed {

  likes: number;
  comments: CommentFeed[];
}

export class CondigForDialog {
  canEdit: boolean;
  buttonText: string;
}


