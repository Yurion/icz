import { Feed } from '../../Classes/Helpers';
import { FeedService } from 'src/app/Services/feed.service';
import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, ReplaySubject, of } from 'rxjs';
import { take, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SelectionFeedService implements OnDestroy {

  private selectedFeed$ = new BehaviorSubject<Feed>(null);
  public selctedFeedObs$ = this.selectedFeed$.asObservable();


  private feedData$ = new ReplaySubject<Feed[]>();
  public feedDataObs$ = this.feedData$.asObservable();

  private updateFeed$ = new ReplaySubject<Feed>(null);
  public updateFeedObs$ = this.updateFeed$.asObservable();


  constructor(private feedServ: FeedService) { }

  ngOnDestroy(): void {
    this.selectedFeed$.unsubscribe();
    this.feedData$.unsubscribe();
    this.updateFeed$.unsubscribe();
  }


  selectFeed(feed: Feed) {
    this.selectedFeed$.next(feed);
  }
  clearSelection() {
    this.selectedFeed$.next(null);
  }
  updatedFeed(curentlyUpdated: Feed) {
    this.updateFeed$.next(curentlyUpdated);
  }
  getNewData() {
    this.feedServ.getFeeds().pipe(
      take(1),
      catchError(x => {
        console.log(x);
        return of(null);
      })).subscribe((x: Feed[]) => {
        if (x) {
          this.feedData$.next(x)
          if (this.selectedFeed$.value != null) {
            this.selectFeed(x.find(s => s.id == this.selectedFeed$.value.id))
          }
        }
      })
  }
}
