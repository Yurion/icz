import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

export class InformativeMessage {
  severity: string;
  summary: string;
  detail: string;
}


@Injectable({
  providedIn: 'root'
})
export class InformativeService {

  private inform$ = new Subject<InformativeMessage>();
  public informObs$ = this.inform$.asObservable();
  constructor() { }

  createToadMessage(msg: InformativeMessage) {
    this.inform$.next(msg)
  }
}
