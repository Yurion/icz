import { Feed } from './../Helpers/Classes/Helpers';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CommentFeed } from '../Helpers/Classes/Helpers';

@Injectable({
  providedIn: 'root'
})

export class FeedService {

  private getFeedsUrl = environment.ApiUrl + '/feeds';
  private getCreateNewFeedUrl = environment.ApiUrl + '/feed'
  //after second / id of feeed needs to be typed
  private getDeleteFeedUrl = environment.ApiUrl + '/feed/'
  //after second / id of feeed needs to be typed and /comment after the id
  private getCreateNewCommentUrl = environment.ApiUrl + '/feed/'
  //after second / id of feeed needs to be typed
  private getFeedByIdUrl = environment.ApiUrl + '/feed/'
   //after second / id of comment needs to be typed
  private getDeletFeedCommentUrl = environment.ApiUrl + '/comment/'
   //after second / id of feed needs to be typed
  private getSaveFeedUrl = environment.ApiUrl + '/feed/'
  constructor(
    private http: HttpClient) { }


  public getFeeds(): Observable<Feed[]> {
    return this.http.get<Feed[]>(this.getFeedsUrl);
  }
  public createNewFeed(saveFeed: Feed): Observable<any> {
    return this.http.put<void>(this.getCreateNewFeedUrl, saveFeed);
  }
  public deleteFeed(deleteFeed: Feed): Observable<any> {
    return this.http.delete<void>(this.getDeleteFeedUrl + "" + deleteFeed.id);
  }
  public createNewComment(newCom: CommentFeed, idFeed: number) {
    return this.http.put(this.getCreateNewCommentUrl + "" + idFeed + "/comment", newCom);
  }
  public getFeedById(feed: Feed):Observable<Feed> {
    return this.http.get<Feed>(this.getFeedByIdUrl+""+feed.id);
  }
  public deleteFeedCommentById(comment: CommentFeed):Observable<any> {
    return this.http.delete<void>(this.getDeletFeedCommentUrl+""+comment.id);
  }
  public saveFeed(feed: Feed):Observable<any> {
    return this.http.put<void>(this.getSaveFeedUrl+""+feed.id,feed);
  }

}
