import { takeUntil } from 'rxjs/operators';
import { InformativeService } from './Helpers/Services/Informative/informative.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[MessageService]
})
export class AppComponent implements OnInit,OnDestroy {
  title = 'ZadanieICZ';

  destroy$= new Subject();

  constructor(
    private msgSrv:MessageService,
    private inforServ: InformativeService
    ){

  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
  ngOnInit(): void {
    this.inforServ.informObs$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(x=>{
      this.msgSrv.add(x);
    })
  }
}
